# integration-test

## Pré requisitos

Instalação necessárias:
- Golang [1.17+](https://go.dev/doc/install)
- Docker [18.02.0+](https://docs.docker.com/engine/install/)


## Execução

Execute `go test -v monolith_test.go` para inicializar o serviço.

```
emmanuelneri@MacBook-Pro-de-Emmanuel integration-test % go test -v monolith_test.go 
=== RUN   TestMonolith
=== RUN   TestMonolith/should_create_a_customer_and_user
=== RUN   TestMonolith/should_request_a_token
=== RUN   TestMonolith/should_get_created_customer
=== RUN   TestMonolith/should_create_two_products
=== RUN   TestMonolith/should_get_created_products
=== RUN   TestMonolith/should_create_a_order
=== RUN   TestMonolith/should_get_created_order
--- PASS: TestMonolith (2.24s)
    --- PASS: TestMonolith/should_create_a_customer_and_user (1.10s)
    --- PASS: TestMonolith/should_request_a_token (1.10s)
    --- PASS: TestMonolith/should_get_created_customer (0.00s)
    --- PASS: TestMonolith/should_create_two_products (0.01s)
    --- PASS: TestMonolith/should_get_created_products (0.00s)
    --- PASS: TestMonolith/should_create_a_order (0.01s)
    --- PASS: TestMonolith/should_get_created_order (0.00s)
PASS
ok  	command-line-arguments	2.780s
```
