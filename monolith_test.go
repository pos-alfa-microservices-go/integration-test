package monolithtest

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"strings"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/assert"
	"gopkg.in/h2non/baloo.v3"
)

var monololithClient = baloo.New("http://localhost:8080")

func TestMonolith(t *testing.T) {
	var customerId string
	var customerName string
	var userLogin string
	var token string

	var products map[string]string

	var orderId string
	const password = "golang"

	t.Run("should create a customer and user", func(t *testing.T) {
		newCustomerName := fmt.Sprintf("Customer %s", uuid.New())
		newCustomerEmail := fmt.Sprintf("%s@gmail.com", strings.ReplaceAll(newCustomerName, " ", ""))

		assertCustomer := func(res *http.Response, req *http.Request) error {
			var responseMap map[string]interface{}
			if err := json.NewDecoder(res.Body).Decode(&responseMap); err != nil {
				return err
			}

			customerId = responseMap["Id"].(string)
			customerName = responseMap["Name"].(string)
			userMap := responseMap["User"].(map[string]interface{})
			userLogin = userMap["Login"].(string)

			return nil
		}

		monololithClient.Post("/customers").
			JSON(map[string]interface{}{"name": newCustomerName, "email": newCustomerEmail}).
			Expect(t).
			Status(200).
			Type("json").
			AssertFunc(assertCustomer).
			Done()

		assert.NotEmpty(t, customerId)
		assert.Equal(t, newCustomerEmail, userLogin)
	})

	t.Run("should request a token", func(t *testing.T) {
		assertToken := func(res *http.Response, req *http.Request) error {
			var responseMap map[string]interface{}
			if err := json.NewDecoder(res.Body).Decode(&responseMap); err != nil {
				return err
			}

			token = responseMap["Token"].(string)

			return nil
		}

		monololithClient.Post("/login").
			JSON(map[string]interface{}{"login": userLogin, "password": password}).
			Expect(t).
			Status(201).
			Type("json").
			AssertFunc(assertToken).
			Done()

		assert.NotEmpty(t, token)
	})

	t.Run("should get created customer", func(t *testing.T) {
		assertCustomer := func(res *http.Response, req *http.Request) error {
			var responseMap map[string]interface{}
			if err := json.NewDecoder(res.Body).Decode(&responseMap); err != nil {
				return err
			}

			assert.Equal(t, customerId, responseMap["Id"].(string))
			assert.Equal(t, customerName, responseMap["Name"].(string))
			assert.Equal(t, userLogin, responseMap["Email"].(string))

			return nil
		}

		monololithClient.Get("/customers/"+customerId).
			SetHeader("Authorization", "Bearer "+token).
			Expect(t).
			Status(200).
			Type("json").
			AssertFunc(assertCustomer).
			Done()
	})

	t.Run("should create two products", func(t *testing.T) {
		products = make(map[string]string, 0)

		assertProduct := func(res *http.Response, req *http.Request) error {
			var responseMap map[string]interface{}
			if err := json.NewDecoder(res.Body).Decode(&responseMap); err != nil {
				return err
			}

			productId := responseMap["Id"].(string)
			productName := responseMap["Name"].(string)
			products[productId] = productName

			return nil
		}

		for i := 0; i < 2; i++ {
			productName := fmt.Sprintf("Product %d", i)
			rand.NewSource(time.Now().UnixNano())
			value := rand.Float32()

			monololithClient.Post("/products").
				SetHeader("Authorization", "Bearer "+token).
				JSON(map[string]interface{}{"name": productName, "value": value}).
				Expect(t).
				Status(200).
				Type("json").
				AssertFunc(assertProduct).
				Done()
		}

		assert.Equal(t, 2, len(products))
	})

	t.Run("should get created products", func(t *testing.T) {

		for productId, productName := range products {
			assertProduct := func(res *http.Response, req *http.Request) error {
				var responseMap map[string]interface{}
				if err := json.NewDecoder(res.Body).Decode(&responseMap); err != nil {
					return err
				}

				assert.Equal(t, productId, responseMap["Id"].(string))
				assert.Equal(t, productName, responseMap["Name"].(string))

				return nil
			}

			monololithClient.Get("/products/"+productId).
				SetHeader("Authorization", "Bearer "+token).
				Expect(t).
				Status(200).
				Type("json").
				AssertFunc(assertProduct).
				Done()
		}

	})

	t.Run("should create a order", func(t *testing.T) {
		var total string

		assertOrder := func(res *http.Response, req *http.Request) error {
			var responseMap map[string]interface{}
			if err := json.NewDecoder(res.Body).Decode(&responseMap); err != nil {
				return err
			}

			orderId = responseMap["Id"].(string)
			customerMap := responseMap["Customer"].(map[string]interface{})
			assert.Equal(t, customerId, customerMap["Id"].(string))
			assert.Equal(t, customerName, customerMap["Name"].(string))

			items := responseMap["Items"].([]interface{})
			for _, item := range items {
				itemMap := item.(map[string]interface{})

				productMap := itemMap["Product"].(map[string]interface{})
				productName := products[productMap["Id"].(string)]

				assert.NotEmpty(t, productName, productMap["Name"].(string))

				assert.NotEmpty(t, itemMap["UnitValue"].(string))
				assert.Equal(t, float64(2), itemMap["Quantity"].(float64))
			}

			total = responseMap["Total"].(string)

			return nil
		}

		customer := map[string]interface{}{"id": customerId}
		items := make([]interface{}, 0)
		for productId, _ := range products {
			product := map[string]interface{}{"id": productId}
			items = append(items, map[string]interface{}{"product": product, "quantity": 2, "unitValue": 150.50})
		}

		monololithClient.Post("/orders").
			SetHeader("Authorization", "Bearer "+token).
			JSON(map[string]interface{}{"customer": customer, "items": items}).
			Expect(t).
			Status(200).
			Type("json").
			AssertFunc(assertOrder).
			Done()

		totalExpected := decimal.NewFromInt(602).String()
		assert.Equal(t, totalExpected, total)
	})

	t.Run("should get created order", func(t *testing.T) {
		var total string

		assertOrder := func(res *http.Response, req *http.Request) error {
			var responseMap map[string]interface{}
			if err := json.NewDecoder(res.Body).Decode(&responseMap); err != nil {
				return err
			}

			customerMap := responseMap["Customer"].(map[string]interface{})
			assert.Equal(t, customerId, customerMap["Id"].(string))
			assert.Equal(t, customerName, customerMap["Name"].(string))

			items := responseMap["Items"].([]interface{})
			for _, item := range items {
				itemMap := item.(map[string]interface{})

				productMap := itemMap["Product"].(map[string]interface{})
				productName := products[productMap["Id"].(string)]

				assert.NotEmpty(t, productName, productMap["Name"].(string))

				assert.NotEmpty(t, itemMap["UnitValue"].(string))
				assert.Equal(t, float64(2), itemMap["Quantity"].(float64))
			}

			total = responseMap["Total"].(string)

			return nil
		}

		monololithClient.Get("/orders/"+orderId).
			SetHeader("Authorization", "Bearer "+token).
			Expect(t).
			Status(200).
			Type("json").
			AssertFunc(assertOrder).
			Done()

		totalExpected := decimal.NewFromInt(602).String()
		assert.Equal(t, totalExpected, total)
	})

}
